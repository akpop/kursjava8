package de.metafinanz.bsp.person;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.metafinanz.bsp.vertrag.impl.TestDataGenerator;

public class PersonVersuche {

	public static void main(String[] args) {
		System.out.println("Gesamtalter:" + readPersonen().stream().mapToInt(p -> p.getAlter()).sum());		
		
		Stream<String> stream = Stream.of("Eins", "Zwei", "Drei");
		Optional<String> first = stream.findFirst();
		first.ifPresent(System.out::println);
		
		Optional<Person> opt = Optional.of(new Person("Hans", "Meier", "1900", "12345"));	
		String n = opt.map(Person::getNachname).orElse("Name unbekannt!");
		
		Integer gesamtalter = readPersonen().stream().reduce(0, (sum, p) -> sum += p.getAlter(), (sum1, sum2) -> sum1 + sum2);
		System.out.println("Alter=" + gesamtalter);
		
	}
	

	static List<Person> readPersonen() {
		List<Person> result = new ArrayList<>();
		try(Stream<String> lines = Files.lines(TestDataGenerator.getFileFromResources("Personen.txt"), StandardCharsets.UTF_8)) {
			result = lines.map(l -> l.split(",")).map((String[] a) -> new Person(a[1].trim(), a[0].trim(), a[2].trim(), a[3].trim()))
				.collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
}
