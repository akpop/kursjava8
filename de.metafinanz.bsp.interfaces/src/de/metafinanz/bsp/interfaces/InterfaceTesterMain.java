package de.metafinanz.bsp.interfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntBinaryOperator;
import java.util.stream.Stream;

public class InterfaceTesterMain {

	public static void main(String[] args) {
		InterfaceTester interfaceTester = new InterfaceTester();
		
		interfaceTester.methode("1. Methode");
		interfaceTester.nochEineMethode("2. Methode");
		BinaryOperator<Integer> bo = (a, b) -> a + b;
		IntBinaryOperator io = (int i, int j) -> i + j;
		
		Consumer<String> printFormatted =
				(s) -> {if(s.length()>0) {
								System.out.println(
									Character.toUpperCase(s.charAt(0)) + s.substring(1));
							}
						};
		printFormatted.accept("asdf");
		
		List<Person> ps = new ArrayList<>();
		Stream<String> names = ps.stream().<String>map(p -> p.getName());
		
	}
	
	class Person {

		public String getName() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}

}
