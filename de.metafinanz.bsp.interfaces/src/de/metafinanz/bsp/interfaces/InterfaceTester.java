package de.metafinanz.bsp.interfaces;

import de.versuche.EinInterface;

public class InterfaceTester implements EinInterface {

	@Override
	public void methode(String s) {
		System.out.println(this.getClass().getSimpleName() + ": " + s);
	}
	
//	public void nochEineMethode(String s) {
//		System.out.println(this.getClass().getSimpleName() + ": " + s);
//	}

}
