package de.versuche.files;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class HashVersuch {

	public static void main(String[] args) {
//		try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths
//					.get(System.getProperty("user.home") + "/Downloads/"))) {
//			stream.forEach(HashVersuch::calculateHashWithMappedBuffer);
//		}
//		catch (IOException e) {
//			e.printStackTrace();
//		}
		
		try {
			Files.list(Paths.get(System.getProperty("user.home") + "/Downloads/"))
				.filter(f -> !Files.isDirectory(f))
				.forEach(HashVersuch::calculateHashWithMappedBuffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static void calculateHashesWithReadAllBytes(Path path) {
		try {
			Instant start = Instant.now();
			MessageDigest digest = MessageDigest.getInstance("MD5");
			System.out.println(path.getFileName() + ": " + Files.size(path));
			byte[] bytes = Files.readAllBytes(path);

			byte[] d = digest.digest(bytes);
			Instant stop = Instant.now();
			System.out.println(new BigInteger(1, d).toString(16));
			System.out.println("Fil: " + Duration.between(start, stop).toMillis());
//			bytes.clear();
			digest.reset();
		} catch (NoSuchAlgorithmException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	static void calculateHashWithMappedBuffer(Path path) {
		try (FileChannel channel = (FileChannel) Files.newByteChannel(path, StandardOpenOption.READ)) {
			System.out.println(path.getFileName() + ": " + Files.size(path));
			MessageDigest digest = MessageDigest.getInstance("MD5");
			Instant start = Instant.now();
			MappedByteBuffer buffer = channel.map(MapMode.READ_ONLY, 0, Files.size(path));
			digest.update(buffer);
			byte[] d = digest.digest();
			Instant stop = Instant.now();
			System.out.println(new BigInteger(1, d).toString(16));
			System.out.println(new String(d));
			System.out.println("Map: " + Duration.between(start, stop).toMillis());
			buffer.clear();
			digest.reset();
		} catch (IOException | NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static void kleiner() {
		Path path = Paths.get(System.getProperty("user.dir"), "/Downloads/", "eclipse-rcp-luna-SR1-win32-x86_64.zip");
		try (FileChannel channel = (FileChannel) Files.newByteChannel(path, StandardOpenOption.READ)) {
			MappedByteBuffer buffer = channel.map(MapMode.READ_ONLY, 0, Files.size(path));

			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(buffer);
			byte[] d = digest.digest();
			System.out.println(new BigInteger(1, d).toString(16));
		} catch (IOException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
	
	

	static void bufferedReader() {
		Path path = Paths.get("");
		try(BufferedReader br = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
		    for(String line; (line = br.readLine()) != null; ) {
		        // 
		    }
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	static void readAllLines() {
		Path path = Paths.get("");
		try {
			List<String> lines = Files.readAllLines(path);
			for (String line : lines) {
				// verwende line
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static void linesStream() {
		Path path = Paths.get("");
		try {
			Stream<String> lines = Files.lines(path, StandardCharsets.UTF_8);
			lines.forEach(line -> System.out.println(line));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
			
	static void directoryStream() {
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths
					.get("/home/akpop/Downloads/"))) {
			stream.forEach(f -> System.out.println(f.getFileName()));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	

}
