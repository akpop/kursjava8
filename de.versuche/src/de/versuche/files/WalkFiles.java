package de.versuche.files;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

public class WalkFiles {
	public static void main(String[] args) {
		String dir = System.getProperty("user.dir");
		try {
			Files.walk(Paths.get(dir)).filter(f -> f.toFile().isDirectory()).forEach(System.out::println);
			Stream<Path> fileWalk = Files.walk(Paths.get(dir));
			long sum = fileWalk.mapToLong(f -> f.toFile().length()).sum();
////			try {
//			sum = 	fileWalk
//					.peek(System.out::println)
//					.mapToLong(f -> {try {
//						return Files.size(f);
//					} catch (Exception e) {
//						e.printStackTrace();
//						return 0;
//					}}).sum();
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			System.out.println(sum);
			zeug(Paths.get(dir).toFile(), ".txt");
			zeug2();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static File[] zeug(File aDir, String ext) {
		if(!aDir.isDirectory()) {
			throw new IllegalArgumentException();
		}
		aDir.list((d, s) -> s.endsWith(ext));
		return aDir.listFiles(File::isDirectory);
	}
	
	static void zeug2() {
		String[] names = {"Eins", "Zwei", "Drei"};
		List<Runnable> runners = new ArrayList<>();
		for (final String name : names) {
			runners.add(() -> System.out.println(name));
		}
		
		Arrays.stream(names).map(n -> n.length()).forEach(System.out::println);
		
		Arrays.stream(new int[]{1, 2, 3}).mapToObj(i -> Integer.toString(i)).forEach(System.out::println);
//		
//		runners.map(r -> );
//		Arrays.stream(names).map(n -> {runners.add(() -> System.out.println(n + " x")); return runners;})
//			.forEach(r -> new Thread((Runnable) r).start());
		
		
		
//		for (Runnable runnable : runners) {
//			new Thread(runnable).start();
//		}
		
//		for (int i = 0; i < names.length; i++) {
//			runners.add(() -> System.out.println(names[i]));
//			
//		}
		String n2 = "asdf";
		Arrays.stream(names).forEach(n -> System.out.println(n2));
		
//		n2 = "qwer"; // auskkommentioert: Compile-Fehler
	}
	
	static void $() {
		// Ullenboom
		Object[] words = { " ", '3', null, "2", 1, "" };
		Arrays.stream( words )
		      .filter( Objects::nonNull )
		      .map( Objects::toString )
		      .map( String::trim )
		      .filter( s -> ! s.isEmpty() )
		      .map( Integer::parseInt )
		      .sorted()
		      .forEach( System.out::println );   // 1 2 3
		
	}
	
	static class MeinMap implements Function<Runnable, Thread>{

		@Override
		public Thread apply(Runnable r) {
			return new Thread(r);
		}
		
	}
	
}
 