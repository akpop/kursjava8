package de.versuche;

public class Hello {
	static String s;
	
	  Runnable r1 = () -> { System.out.println(this + s); };
	  Runnable r2 = () -> { System.out.println(toString() + s); };
	  
	  Runnable r3 = new Runnable() {
		@Override
		public void run() {
			System.out.println(Hello.this);
		}
	  };
	  
	  Runnable r4 = new Runnable() {
			@Override
			public void run() {
				System.out.println(this.toString());
			}
			public String toString() {return "asdfasdf";};
		  };

	  public String toString() { return "Hello, world!"; }

	  public static void main(String... args) {
		  s = " asdf";
	    new Hello().r1.run();
	    s = "wwww";
	    new Hello().r2.run();
	    new Hello().r3.run();
	    new Hello().r4.run();
	  }
	}