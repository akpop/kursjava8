package de.versuche.parallel;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class Sort {

	public static void main(String[] args) {
		int max = 1000000;
		List<String> values = new ArrayList<>(max);
		for (int i = 0; i < max; i++) {
		    UUID uuid = UUID.randomUUID();
		    values.add(uuid.toString());
		}
		
		sortListWithStream("parallel", () -> values);
		sortListWithStream("standard", () -> values);
		sortListWithStream("standard", () -> values);
		sortListWithStream("standard", () -> values);
		sortListWithStream("parallel", () -> values);
		sortListWithStream("parallel", () -> values);
		sortListWithStream("parallel", () -> values);
		sortListWithStream("standard", () -> values);

	}
	
//	static void sortParallel(List<String> values) {
//		long t0 = System.nanoTime();
//		
//		long count = values.parallelStream().sorted().count();
//		System.out.println(count);
//		
//		long t1 = System.nanoTime();
//		
//		long millis = TimeUnit.NANOSECONDS.toMillis(t1 - t0);
//		System.out.println(String.format("parallel sort took: %d ms", millis));
//		
//	}
	
	static void sortListWithStream(String message, SortStringStream s) {
		long t0 = System.nanoTime();
		Stream<String> stream = null;
		switch (message) {
		case "parallel":
			stream = s.sort().parallelStream();
			break;
		case "standard":
			stream = s.sort().stream();
			break;
		default:
			throw new IllegalStateException("Unbekannt: " + message);
		}
		long count = stream.sorted().count();
		System.out.println(count);
		
		long t1 = System.nanoTime();
		
		long millis = TimeUnit.NANOSECONDS.toMillis(t1 - t0);
		System.out.println(String.format(message + ": sort took: %d ms", millis));
	}
	
	static interface SortStringStream {
		List<String> sort();
	}

}
