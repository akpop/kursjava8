package de.versuche;

import java.util.ArrayList;
import java.util.Random;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;

public class IntStreamExample {

	public static void main(String[] args) {
//		Object o = IntStream.generate(()->23).limit(10).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
		
		IntPredicate groesser65 = (i -> i > 65);
		IntPredicate kleiner128 = (i -> i < 105);
		IntPredicate buchstaben = groesser65.and(kleiner128);
		
		Random random = new Random();
		random.nextInt(255);
		String s = IntStream.generate(() -> random.nextInt(255))
				.filter(buchstaben).
				limit(20)
				.collect(StringBuilder::new, (sb, i) -> sb.append((char)i), StringBuilder::append).toString();
		System.out.println(s);
		
		//anderes Bsp
		String string = IntStream.range(0, 3)
				.collect(()-> new StringBuilder("asdf"), (o, i) -> {System.out.printf("i=%d, o=%s%n",i,o);}, StringBuilder::append).toString();
		System.out.println(string);
	}
	
	public static void schritt1() {
		IntStream.generate(()->23).limit(10);
		
	}
	
	public static void schritt2() {
		
		/*final*/ Random random = new Random();
		random.nextInt(255);
		IntStream.generate(() -> random.nextInt(255))
				.limit(20);
	}
	
	public static void schritt3() {
		Random random = new Random();
		random.nextInt(255);
		String s = IntStream.generate(() -> random.nextInt(255))
			.limit(20)
			.collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString();
		System.out.println(s);
	}
	
	public static void schritt4() {
		Random random = new Random();
		random.nextInt(255);
		String s = IntStream.generate(() -> random.nextInt(255))
			.limit(20)
			.collect(StringBuilder::new, (sb, i) -> sb.append((char)i), StringBuilder::append).toString();
		System.out.println(s);
	}
	
	public static void schritt5() {
		IntPredicate lowerLimit = (i -> i > 65);
		IntPredicate kleiner128upperLimit = (i -> i < 105);
		IntPredicate buchstaben = lowerLimit.and(kleiner128upperLimit);
		
		Random random = new Random();
		random.nextInt(255);
		String s = IntStream.generate(() -> random.nextInt(255))
			.filter(buchstaben).
			limit(20)
			.collect(StringBuilder::new, (sb, i) -> sb.append((char)i), StringBuilder::append).toString();
		System.out.println(s);
	}

}
