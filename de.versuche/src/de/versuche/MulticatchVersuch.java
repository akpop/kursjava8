package de.versuche;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class MulticatchVersuch {
	
	private String xy;

	public static void main(String[] args) {
		versuch();
	}
	
	void machWas() throws IOException, MyOwnException {
		Path p = Paths.get("");
		String z = "13";
		String xy = "10";
		try(Stream<String> lines = Files.lines(p)) {
				int i = lines.map(s -> Integer.parseInt(s + xy + z)).findFirst().get().intValue();
				wirftException(i);
//				xy = "11";
		} catch (IOException | MyOwnException e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	void wirftException(int i) throws MyOwnException {
		if(i < 0) {
			throw new MyOwnException();
		}
	}
	
	private class MyOwnException extends Exception {
		
	}
	
	static void versuch() {
		String[] names = { "Peter", "Paul", "Mary" };
		List<Runnable> runners = new ArrayList<>();
		for (String name : names)
			runners.add(() -> System.out.println(name));
		runners.stream().forEach(r -> r.run());
	}

}
