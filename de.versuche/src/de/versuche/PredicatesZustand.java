package de.versuche;

import java.util.Objects;
import java.util.function.Predicate;

public enum PredicatesZustand {
	
	NONNULL(Objects::nonNull),
	NOTEMPTY(NONNULL.predicate.and(String::isEmpty).negate()),
	STARTSWITHCAPITAL(NOTEMPTY.predicate.and(s -> (Character.isUpperCase(s.charAt(0))))),
	;
	
	private final Predicate<String> predicate;
	
	private PredicatesZustand(Predicate<String> p) {
		predicate = p;
		
	}
	

}
