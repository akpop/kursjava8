package de.versuche;



public class SortableType implements Comparable<SortableType>{
	
	private String hauptMerkmal;
	private String nebenMerkmal;
	
	public SortableType(String s1, String s2) {
		this.hauptMerkmal = s1;
		this.nebenMerkmal = s2;
	}

	public String getS1() {
		return hauptMerkmal;
	}

	public String getS2() {
		return nebenMerkmal;
	}

	@Override
	public int compareTo(SortableType o) {
		return getS1().compareTo(o.getS1());
	}
	
	@Override
	public String toString() {
		return String.format("H: %s, N: %s", hauptMerkmal, nebenMerkmal);
	}

}
