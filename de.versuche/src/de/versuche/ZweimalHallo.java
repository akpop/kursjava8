package de.versuche;

public class ZweimalHallo {
	public String value;
	
	Runnable r1 = () -> System.out.println(this);
	Runnable r2 = new Runnable() {
		@Override
		public void run() {
			System.out.println(this);
		}
	};

	@Override
	public String toString() {
		return value;
	}
	
	public static void main(String[] args) {
		ZweimalHallo obj = new ZweimalHallo();
		obj.value = "Hallo";
		obj.r1.run(); //--> Hallo
		obj.r2.run(); //--> ...ZweimalHallo$1@5ca881b5
	}
}
