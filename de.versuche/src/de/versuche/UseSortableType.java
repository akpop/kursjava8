package de.versuche;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class UseSortableType {

	public static void main(String[] args) {
		List<SortableType> list = Arrays.asList(
				new SortableType("Eins A", "Eins B"),
				new SortableType("Zwei A", "Zwei B"),
				new SortableType("Drei A", "Drei B"),
				new SortableType("Y A", "X B")
			);
		
		list.sort(new Comparator<SortableType>() {

			@Override
			public int compare(SortableType t1, SortableType t2) {
				return t1.getS2().compareTo(t2.getS2());
			}
		});
		
		System.out.println(Arrays.toString(list.toArray()));
		Collections.sort(list);
		System.out.println(list);
		
	}

}
