package de.metafinanz.bsp.vertrag;


public interface Versvertrag extends Persistent {
	String getSparte_Eigen();
	void setSparte_Eigen(String value);
	
	String getPollfnr();
	void setPollfnr(String nr);
	
	String getId();
	void setId(String id);
	
	ChildPersistentRelation<ProdAus> getProdAus();
}
