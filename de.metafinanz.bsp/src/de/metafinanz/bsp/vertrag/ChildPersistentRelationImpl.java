package de.metafinanz.bsp.vertrag;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class ChildPersistentRelationImpl<T extends Persistent> extends AbstractCollection<T> implements ChildPersistentRelation<T> {
	
	private List<T> list = new ArrayList<>();
	private Comparator<T> comparator;
	
	public void addElement(T el) {
		list.add(el);
	}

	@Override
	public T get(int i) {
		
		return null;
	}
	
	@Override
	public void setComparator(Comparator<T> comparator) {
		this.comparator = comparator;
	}


	@Override
	public T create(IInitializer<T> initializer) {
		return null;
	}

	@Override
	public T getFirstElement() {
		if(!list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public Iterator<T> iterator() {
		return list.iterator();
	}
	
	@Override
	public int size() {
		return list.size();
	}

}
