package de.metafinanz.bsp.vertrag;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;

public interface Persistent {
	
	static String toStringBuilder(Persistent p) {
		class Wrapper {
			public String wrap(Field f, Persistent p) {
				try {
					f.setAccessible(true);
					return Objects.toString(f.get(p), "null");
				} catch (IllegalArgumentException | IllegalAccessException e) {
					throw new RuntimeException(e);
				} finally {
					f.setAccessible(false);
				}
			}
		}
		Wrapper w = new Wrapper();
		
		return String.join(", ", (String[]) Arrays.stream(p.getClass().getDeclaredFields())
//					.filter(f -> !f.getName().contains("vers"))
					.map(f -> f.getName() + "=" + w.wrap(f, p))
					.toArray(String[]::new)
				);
	}

}
