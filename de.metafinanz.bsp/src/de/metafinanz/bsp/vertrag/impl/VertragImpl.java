package de.metafinanz.bsp.vertrag.impl;

import de.metafinanz.bsp.vertrag.ChildPersistentRelation;
import de.metafinanz.bsp.vertrag.Persistent;
import de.metafinanz.bsp.vertrag.Versvertrag;
import de.metafinanz.bsp.vertrag.Vertrag;

public class VertragImpl implements Vertrag {
	
	private ChildPersistentRelation<Versvertrag> versVertraege;
	private String status;
	private String hspEigen;
	private String pollfnr;
	private String personKey;

	
	@Override
	public String getPllfnr() {
		return pollfnr;
	}

	@Override
	public String getVertrstatus() {
		return status;
	}

	@Override
	public void setVertrstatus(String status) {
		this.status = status;
	}

	@Override
	public String getHsp_Eigen() {
		return hspEigen;
	}

	@Override
	public void setHsp_Eigen(String value) {
		this.hspEigen = value;
	}

	
	@Override
	public void setPersonKey(String key) {
		this.personKey = key;
	}
	
	@Override
	public String getPersonKey() {
		return personKey;
	}

	@Override
	public ChildPersistentRelation<Versvertrag> getVersvertrag() {
		
		return versVertraege;
	}
	
	public static class VertragBuilder {
		Vertrag instance = new VertragImpl();
		
		public VertragBuilder hspEigen(String hspEigen) {
			instance.setHsp_Eigen(hspEigen);
			return this;
		}
		
		
	}
	
	@Override
	public String toString() {
//		StringBuilder sb = new StringBuilder();
//		sb.append("Pollfnr=").append(pollfnr).append(", Status=").append(status).append(", HSP_Eigen=").append(hspEigen);
//		return sb.toString();
		return Persistent.toStringBuilder(this);
	}


}
