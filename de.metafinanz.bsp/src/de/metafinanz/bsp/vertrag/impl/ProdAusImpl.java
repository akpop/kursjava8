package de.metafinanz.bsp.vertrag.impl;

import de.metafinanz.bsp.vertrag.Persistent;
import de.metafinanz.bsp.vertrag.ProdAus;

public class ProdAusImpl implements ProdAus {
	
	private String sumartbez;
	private String summe;
	private String vvid;

	@Override
	public String getSumartbez() {
		return sumartbez;
	}

	@Override
	public void setSumartbez(String s) {
		this.sumartbez = s;
	}

	@Override
	public Double getSumme() {
		return Double.valueOf(summe);
	}

	@Override
	public void setSumme(String summe) {
		this.summe = summe;
	}

	@Override
	public String getVvId() {
		return vvid;
	}

	@Override
	public void setVvId(String id) {
		vvid = id;
	}
	
	@Override
	public String toString() {
		return Persistent.toStringBuilder(this);
	}

}
