package de.metafinanz.bsp.vertrag.impl;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.xml.transform.stream.StreamSource;

import de.metafinanz.bsp.vertrag.ChildPersistentRelationImpl;
import de.metafinanz.bsp.vertrag.Persistent;
import de.metafinanz.bsp.vertrag.ProdAus;
import de.metafinanz.bsp.vertrag.Versvertrag;
import de.metafinanz.bsp.vertrag.Vertrag;

public class TestDataGenerator {
	
	public static Vertrag getVertrag(String[] values) {
		Vertrag result = new VertragImpl();
		try {
			Field field = result.getClass().getDeclaredField("versVertraege");
			field.setAccessible(true);
			field.set(result, new ChildPersistentRelationImpl<Versvertrag>());
			field.setAccessible(false);
			int HSP_POS = 4;
			result.setHsp_Eigen(values[HSP_POS ]);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static Vertrag createVertrag(String values) {
		Vertrag result = new VertragImpl();
		try {
			Stream<Field> fieldStream = Stream.of(result.getClass().getDeclaredFields());
			Field field = result.getClass().getDeclaredField("versVertraege");
			field.setAccessible(true);
			field.set(result, new ChildPersistentRelationImpl<Versvertrag>());
			field.setAccessible(false);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static <T> void findMethod_Exception(Persistent p, String methodName, String value) {
		Stream<Field> fieldStream = Stream.of(p.getClass().getDeclaredFields());
		Optional<Field> any = fieldStream.filter(f -> f.getName().toUpperCase().contains(methodName.toUpperCase())).findAny();
		any.ifPresent(f -> {try {
			f.set(p, value);
		} catch (Exception e) {
			e.printStackTrace();
		}});
	}
	
	public static <T> void findMethod(Persistent p, String methodName, String value) {
		Stream<Field> fieldStream = Stream.of(p.getClass().getDeclaredFields());
		Optional<Field> any = fieldStream.filter(f -> f.getName().toUpperCase().contains(methodName.toUpperCase())).findAny();
		any.ifPresent(f -> setPrivateField(f, p, value));
	}
	
	private static void setPrivateField(Field f, Persistent p, String value) {
		f.setAccessible(true);
		try {
			f.set(p, value);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException(e.getMessage());
		}
		f.setAccessible(false);
	}
	
	public static Path getFileFromResources(String name) {
		StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
		URL resource = stackTraceElement.getClass().getResource("/" + name);
//		URL resource = TestDataGenerator.class.getResource("/" + name);
		try {
			return Paths.get(resource.toURI());
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getLocalizedMessage());
		}
	}
	
	public static void readTestdataFile() {
		try {
			List<Object> list = Files.readAllLines(getFileFromResources("TestdatenVertrag.txt"), StandardCharsets.UTF_8).stream()
				.filter(l -> !(l.startsWith("#")))
				.map(line -> buildpersistent(line)).collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void streamTestdataFile(String dataFile) {
		try {
			Files.lines(getFileFromResources(dataFile))
				.filter(l -> !(l.startsWith("#")))
				.map(line -> buildpersistent(line)).collect(Collectors.toList()); //.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	static void iterateFields(Persistent vertrag, String[] fieldNames, String[] split) {
		if(split.length != fieldNames.length + 1) {
			throw new IllegalStateException("Fehler in Testadeten-Datei? " + Arrays.toString(split));
		}
		IntStream.range(0, fieldNames.length).forEach(i -> findMethod(vertrag, fieldNames[i], split[i + 1]));
	}
	
	
	
	private static Persistent buildpersistent(String line) {
		String[] split = line.split(", ");
		Stream<String> stream = Stream.of(split);
		String first = stream.findFirst().get();
		
		List<Vertrag> vertraege = new ArrayList<>();
		List<Versvertrag> versVertraege = new ArrayList<>();
		List<ProdAus> prodauese = new ArrayList<>();
		
		switch (first) {
		case "Vertrag":
			Vertrag v = new VertragImpl();
			iterateFields(v, new String[] {"pollfnr", "Stadium", "Status", "HSP", "personKey"}, split);
			System.out.println("Neuer Vertrag: " + v);
			vertraege.add(v);
			break;
		
		case "Versvertrag":
			Versvertrag vv = new VersvertragImpl();
			iterateFields(vv, new String[] {"sparte_Eigen", "pollfnr", "id"}, split);
			System.out.println("Neuer Versvertrag: " + vv);
			versVertraege.add(vv);
			break;
		case "ProdAus":
			ProdAus pa = new ProdAusImpl();
			iterateFields(pa, new String[] {"sumartbez", "summe", "vvid"}, split);
			System.out.println("Neue Prodaus: " + pa);
			prodauese.add(pa);
			break;
		default:
			break;
		}
		
		vertraege.forEach(action);
		
		
		return new Persistent() {
			@Override
			public String toString() {
				return Arrays.stream(split).findFirst().get();
			}
		};
	}

	public static Persistent createPersistentFromLine(String line) {
		Arrays.stream(line.split(", ")).forEach(System.out::println);
		return null;
	}
	
	
	
	public static void main(String[] args) {
		String rn = "TestdatenVertrag.txt";
		streamTestdataFile(rn);
	}

}
