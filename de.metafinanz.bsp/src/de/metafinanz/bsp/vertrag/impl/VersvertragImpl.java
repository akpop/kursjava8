package de.metafinanz.bsp.vertrag.impl;

import de.metafinanz.bsp.vertrag.ChildPersistentRelation;
import de.metafinanz.bsp.vertrag.Persistent;
import de.metafinanz.bsp.vertrag.ProdAus;
import de.metafinanz.bsp.vertrag.Versvertrag;

public class VersvertragImpl implements Versvertrag {
	
	private ChildPersistentRelation<ProdAus> prodAuese;
	private String sparte_Eigen;
	private String pollfnr;
	private String id;

	@Override
	public String getSparte_Eigen() {
		return sparte_Eigen;
	}

	@Override
	public void setSparte_Eigen(String value) {
		sparte_Eigen = value;
	}

	@Override
	public ChildPersistentRelation<ProdAus> getProdAus() {
		return prodAuese;
	}

	@Override
	public String getPollfnr() {
		return pollfnr;
	}

	@Override
	public void setPollfnr(String nr) {
		pollfnr = nr;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return Persistent.toStringBuilder(this);
	}
	
}
