package de.metafinanz.bsp.vertrag;

import java.util.Collection;

public interface PersistentRelation<T extends Persistent> extends Collection<T>  {
	
	public T create(IInitializer<T> initializer);
	
	public boolean add(T obj);
	
	public T getFirstElement();

}
