package de.metafinanz.bsp.vertrag;

public interface IInitializer<T> {
	
	public void init(T object);

}
