package de.metafinanz.bsp.vertrag;

public interface ProdAus extends Persistent {
	
	String getSumartbez();
	void setSumartbez(String s);

	Double getSumme();
	void setSumme(String summe);
	
	String getVvId();
	void setVvId(String id);
	
}
