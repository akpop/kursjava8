package de.metafinanz.bsp.vertrag;

import java.util.Comparator;

public interface ChildPersistentRelation<T extends Persistent> extends PersistentRelation<T> {
	
	public T get(int i);
	
	public void setComparator(final Comparator<T> comparator);
	
	public void addElement(T el);

}
