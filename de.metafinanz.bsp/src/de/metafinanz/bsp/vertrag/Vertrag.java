package de.metafinanz.bsp.vertrag;

public interface Vertrag extends Persistent {
	
	String getPllfnr();
	
	String getVertrstatus();
	
	void setVertrstatus(String status);
	
	String getHsp_Eigen();
	
	void setHsp_Eigen(String value);
	
	ChildPersistentRelation<Versvertrag> getVersvertrag();
	
	void setPersonKey(String key);
	
	String getPersonKey();

}
