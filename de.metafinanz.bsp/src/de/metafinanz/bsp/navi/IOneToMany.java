package de.metafinanz.bsp.navi;

public interface IOneToMany<O, M> {
	
	INavigation<M> listChildren();

}
