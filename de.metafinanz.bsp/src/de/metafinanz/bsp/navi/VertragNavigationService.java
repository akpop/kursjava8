package de.metafinanz.bsp.navi;

import java.util.Collection;

import de.metafinanz.bsp.vertrag.Versvertrag;
import de.metafinanz.bsp.vertrag.Vertrag;

public class VertragNavigationService implements INavigation<Vertrag> {
	
	private Vertrag vertrag;


	public VertragNavigationService(Vertrag v){
		this.vertrag = v;
		
	}


	@Override
	public <C> Collection<C> getResult() {
		return (Collection<C>) vertrag.getVersvertrag();
	}


	@Override
	public <C> INavigation<C> getListOf(IOneToMany<Vertrag, C> children) {
		return children.listChildren();
	}
	
}
