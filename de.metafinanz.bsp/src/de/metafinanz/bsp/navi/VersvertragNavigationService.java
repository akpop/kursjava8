package de.metafinanz.bsp.navi;

import java.util.Collection;

import de.metafinanz.bsp.vertrag.Versvertrag;

public class VersvertragNavigationService implements INavigation<Versvertrag> {

	private Versvertrag versVertrag;

	public VersvertragNavigationService(Versvertrag versVertrag) {
		this.versVertrag = versVertrag;

	}

	@Override
	public <C> Collection<C> getResult() {
		return (Collection<C>) versVertrag.getProdAus();
	}

	@Override
	public <C> INavigation<C> getListOf(IOneToMany<Versvertrag, C> children) {
		return children.listChildren();
	}

}
