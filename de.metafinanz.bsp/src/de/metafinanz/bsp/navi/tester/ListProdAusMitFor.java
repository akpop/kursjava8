package de.metafinanz.bsp.navi.tester;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

import de.metafinanz.bsp.vertrag.ProdAus;
import de.metafinanz.bsp.vertrag.Versvertrag;
import de.metafinanz.bsp.vertrag.Vertrag;
import de.metafinanz.bsp.vertrag.impl.TestDataGenerator;

public class ListProdAusMitFor {

	public static void main(String[] args) {
//		Vertrag vertrag = TestDataGenerator.getVertrag();
//		double sum = 0;
//		for (Versvertrag vv : vertrag.getVersvertrag()) {
//			for (ProdAus pa : vv.getProdAus()) {
//				if("".equals(pa.getSumartbez())) {
//					sum += pa.getSumme();
//				}
//			}
//		}
//		System.out.println("Summe: " + sum);
		
		Predicate<String> nonNull = Objects::nonNull;
		
		Predicate<String> p = (nonNull.and(String::isEmpty));
		
		List<Vertrag> vertraege = new ArrayList<>();
		for (Vertrag vertrag : vertraege) {
			if("EinStatus".equals(vertrag.getVertrstatus())) {
				tuWasMit(vertrag);
			}
		}
		
		vertraege.forEach(v -> {
			if("EinStatus".equals(v.getVertrstatus()))
				tuWasMit(v);
		});
		
		Map<String, Predicate<String>> predicateMap = new HashMap<>();
		predicateMap.put("notEmpty", s -> s.length() > 0);
		
		
		Stream.of(1, 2, 3).forEach(System.out::println);
		Stream.of("Eins", "Zwei", "Drei").forEach(System.out::println);
		
		
		vertraege.stream()
			.filter(v -> "EinStatus".equals(v.getVertrstatus()))
			.forEach(v -> tuWasMit(v));
		
		vertraege.stream()
			.filter(v -> "EinStatus".equals(v.getVertrstatus()))
			.forEach(ListProdAusMitFor::tuWasMit);
		
		vertraege.stream()
			.filter(statusIsEinStatus)
			.forEach(ListProdAusMitFor::tuWasMit);
		
	}

	
	private static final Predicate<Vertrag> statusIsEinStatus = (v -> "EinStatus".equals(v.getVertrstatus()));

	private static void tuWasMit(Vertrag vertrag) {
		// TODO Auto-generated method stub
		
	}
}
