package de.metafinanz.bsp.navi.tester;

import static de.metafinanz.bsp.navi.ChildrenLister.versvertrag;
import static de.metafinanz.bsp.navi.ChildrenLister.prodaus;

import java.util.Collection;

import de.metafinanz.bsp.navi.INavigation;
import de.metafinanz.bsp.navi.VertragNavigationService;
import de.metafinanz.bsp.vertrag.Vertrag;
import de.metafinanz.bsp.vertrag.impl.TestDataGenerator;

public class Tester {
	
	public static INavigation<Vertrag> from(Vertrag v) {
		return new VertragNavigationService(v);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Vertrag vertrag = TestDataGenerator.getVertrag(null);
		
		Collection<Object> result = from(vertrag).getListOf(versvertrag()).getListOf(prodaus()).getResult();
		result.stream().forEach(System.out::println);
		
	}

}
