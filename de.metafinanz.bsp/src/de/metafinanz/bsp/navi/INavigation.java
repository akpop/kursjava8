package de.metafinanz.bsp.navi;

import java.util.Collection;

public interface INavigation<T> {

	<C> Collection<C> getResult();
	
	<C> INavigation<C> getListOf(IOneToMany<T, C> children);
	
}
