package de.metafinanz.bsp.navi;

import de.metafinanz.bsp.vertrag.ProdAus;
import de.metafinanz.bsp.vertrag.Versvertrag;
import de.metafinanz.bsp.vertrag.Vertrag;

public class ChildrenLister {
	
	public static IOneToMany<Vertrag, Versvertrag> versvertrag() {
		
		return new IOneToMany<Vertrag, Versvertrag>() {

			@Override
			public INavigation<Versvertrag> listChildren() {
				return null;
			}
			
		};
	}
	
	public static IOneToMany<Versvertrag, ProdAus> prodaus() {
		
		return new IOneToMany<Versvertrag, ProdAus>() {

			@Override
			public INavigation<ProdAus> listChildren() {
				return null;
			}
			
		};
	}
	

}
