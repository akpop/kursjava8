package de.metafinanz.bsp.navi;

import de.metafinanz.bsp.vertrag.Versvertrag;
import de.metafinanz.bsp.vertrag.Vertrag;

public class VersvertragMapper implements IOneToMany<Vertrag, Versvertrag>{
	
	private Versvertrag versVertrag;

	public VersvertragMapper(Versvertrag versVertrag) {
		this.versVertrag = versVertrag;
	}

	@Override
	public INavigation<Versvertrag> listChildren() {
		return new VersvertragNavigationService(versVertrag);
	}

}
