package de.metafinanz.bsp.plz;

/**
 * Dummy-Postleitzahlenklasse. Idee: Mehrere Plzs sortierbar in Abhängigkeit zu einer Ausgangs-Plz bezüglich Enfernung. 
 *
 */
public class Plz {
	private final String plz;
	private int x;
	private int y;
	

	public Plz(String plz, int x, int y) {
		this.plz = plz;
		this.x = x;
		this.y = y;
	}
	
	public String getPlz() {
		return plz;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public double getDistanzTo(Plz otherPlz) {
		int x = otherPlz.getX() - this.getX();
		int y = otherPlz.getY() - this.getY();
//		double xx = 0;
//		Math.sqrt(() -> {return 1 + xx;});
		return Math.sqrt(x*x + y*y);
	}
	
	@Override
	public String toString() {
		return plz;
	}
	
	public static Plz createPlz(String plz) {
		
		return new Plz(plz, 0, 0);
	}
}
