package de.metafinanz.bsp.person;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.Year;
import java.util.Objects;

import de.metafinanz.bsp.plz.Plz;

public class Person {
	

	private String vorname;
	private String nachname;
	private int geburtsJahr;
	private Plz postleitzahl;
	
	public Person(String vorname, String nachname, String geburtsJahr,
			String postleitzahl) {
		this.vorname = Objects.requireNonNull(vorname);
		this.nachname = Objects.requireNonNull(nachname);
		this.geburtsJahr = Integer.valueOf(Objects.requireNonNull(geburtsJahr));
		this.postleitzahl = Plz.createPlz(Objects.requireNonNull(postleitzahl));
	} 
	
	public String getVorname() {
		return vorname;
	}
	
	public String getNachname() {
		return nachname;
	}
	
	public int getGeburtsJahr() {
		return geburtsJahr;
	}
	
	public Plz getPostleitzahl() {
		return postleitzahl;
	}
	
	public int getAlter() {
		LocalDate birthdate = LocalDate.of(this.getGeburtsJahr(), Month.JULY, 1);
		LocalDate now = LocalDate.now();
		Period jahre = Period.between(birthdate, now);
		return jahre.getYears();
	}
	
	@Override
	public String toString() {
		return this.vorname + " " + this.nachname + " (" + this.geburtsJahr + ")";
	}

	public static void main(String[] args) {
		Person person = new Person("A", "Xy", "1963", "12345");
		System.out.println(person.getAlter());
	}
}
