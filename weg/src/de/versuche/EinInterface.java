package de.versuche;

public interface EinInterface {
	
	public void methode(String s);
	
	default void nochEineMethode(String s) {
		System.out.println("Default-Implementierung " + s);
	}

}
